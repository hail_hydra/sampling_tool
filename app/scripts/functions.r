# author = santiago rodriguez
# created = 2022.05.25
# purpose = functions for COLT
#           Campaigns Ops List Tool
# citations:
# - https://jennybc.github.io/purrr-tutorial/ls12_different-sized-samples.html
# - https://towardsdatascience.com/programming-with-dplyr-a8161c03d947
# - https://shiny.rstudio.com/articles/progress.html

# assumptions
# - data contains an ID field by which to filter nested lists
###################################################
###################################################
###################################################
###################################################
# load libraries
library(tidyverse)
###################################################
###################################################
###################################################
###################################################
# Function definition: compute population-level statistics
# to be used downstream. The main output of this function
# is the stratum weight (Wh). The stratum weight is
# calculated using proportional allocation. The stratum
# weight is used to calculate the sample size per strata (nh)

# The output of this function is a data frame.
# stratum variables
# Nh: stratum size
# Wh: stratum weight using proportional allocation (Nh/N)

# Attributes
# min_n: the minimum sample size needed for a complete stratified sample

stratified_proportional_allocation <- function(data, variables, ...) {
  out <-
    data %>%
    # create strata
    dplyr::group_by(dplyr::across({{ variables }})) %>%
    # calculate strata size (Nh)
    dplyr::summarise(Nh = dplyr::n()) %>%
    dplyr::ungroup() %>%
    # calculate stratum weight (Nh/N)
    dplyr::mutate(
      Wh = Nh / sum(Nh)
    ) %>%
    # sort the strata for downstream processes
    dplyr::arrange(dplyr::across({{ variables }}))
  ###################################################
  # assign attributes
  attr(out, "min_n") <- ceiling(1 / min(out[["Wh"]]))
  ###################################################
  # output
  return(out)
}
###################################################
###################################################
###################################################
###################################################

###################################################
###################################################
###################################################
###################################################
# Function definition: This is a function to create
# a single stratified sample from a provided data set
# using proportional allocation.

# The output of this function is a data frame.

# Attributes
# n: the requested sample size

# Steps:
# 1. Calculate strata sample size using proportional allocation (nh)
# 2. Intermediary step, prepare provided data set to be sampled
# 3. Stratified random sample of size n where sum(nh1 ... nhn) ~ n

stratified_sampling <- function(data, variables, n, Wh, min_n, ...) {
  ###################################################
  # step 0: error handling
  ## if n is less than min n then set n = min n
  if (n < min_n) {
    warning(
      "\n",
      paste("n =", format(n, big.mark = ","), "is too small", sep = " "),
      "\n",
      paste("n should be greater than or equal to", min_n, sep = " "),
      "\n",
      paste("n was set to", min_n, sep = " ")
    )

    n <- min_n
  }
  ###################################################
  # step 1: calculate nh, strata(h) sample size
  ## if Wh * n not an integer then either round down or up
  ### currently rounding down (floor) so that at most n records are sampled
  ### based on survey design and statistical power should consider rounding up (ceiling)
  nh <- floor(Wh * n)
  ###################################################
  # step 2:
  ## aggregate, one row per group_by combination
  ## append nh
  set.seed(123)
  out <-
    data %>%
    # create strata
    dplyr::group_by(dplyr::across({{ variables }})) %>%
    # one record per strata
    tidyr::nest() %>%
    dplyr::ungroup() %>%
    # sort the strata for downstream processes
    dplyr::arrange(dplyr::across({{ variables }})) %>%
    # append nh, needed in following steps sample from strata
    dplyr::mutate(nh = nh) %>%
    # sample nh records from strata(h)
    dplyr::mutate(
      stratified_data = purrr::map2(
        .x = data,
        .y = nh,
        ~ dplyr::slice_sample(.data = .x, n = .y)
      )
    ) %>%
    # drop list of original data
    dplyr::select(-c(data, nh)) %>%
    # unnest to return to regular data frame
    tidyr::unnest(cols = stratified_data)
  ###################################################
  # assign attributes
  ## n, to extract later
  attr(out, "n") <- n
  ###################################################
  # output
  return(out)
}
###################################################
###################################################
###################################################
###################################################
# Function definition: This is a function to create
# sampling statistics from a stratified sample. The
# output of this function is a data frame.

# Steps:
# 1. aggregate by variables
# 2. compute Wh

stratified_sampling_statistics <- function(stratified_sample, variables) {
  stratified_sample %>%
    # create strata
    dplyr::group_by(dplyr::across({{ variables }})) %>%
    # calculate strata sample size (nh)
    dplyr::summarise(nh = dplyr::n()) %>%
    dplyr::ungroup() %>%
    # stratum weight (nh/n)
    dplyr::mutate(
      Wh = nh / sum(nh)
    )
}
###################################################
###################################################
###################################################
###################################################
# Function definition: To create m lists, some looping
# mechanism is required. This function collects the other
# functions to create a single API/ master function.

# Inputs:
# - data: data frame
# - variable(s): the variables to stratify by
# - n: sample size(s)
# - m: the number of lists to create

# Input data types:
# - data: data frame, tibble
# - variable(s): character, quoted or unquoted: either a string or a vector variables
# - n: numeric: either one number or a vector of numbers
# - m: the number of lists to create

COLT_stratified_prop_alloc <- function(data,
                                       variables,
                                       n,
                                       m,
                                       id,
                                       progressBar = NULL,
                                       verbosity = TRUE,
                                       ...) {
  # verbosity
  if (verbosity) message("COLT: Stratified sampling with proportional allocation")
  ###################################################
  # compute Wh
  step_01 <- stratified_proportional_allocation(
    data = data,
    variables = dplyr::all_of(variables)
  )

  # parameter
  min_n <- attr(step_01, "min_n")

  # verbosity
  if (verbosity) message("Step 01: proportional allocation done")
  ###################################################
  # looping mechanism
  ## create empty output lists
  list_output <- vector(mode = "list", length = m)
  names(list_output) <- paste("list", seq_len(m), sep = "_")
  list_statistics <- list_output
  list_requested_n <- list_output

  # verbosity
  if (verbosity) message("Step 02: pre-loop logistics done")
  ###################################################
  # verbosity
  if (verbosity) message("Step 03: start loop")

  ## loop through 1:m lists
  for (i in seq_len(m)) {
    # verbosity
    if (verbosity) message(paste("Step 03: loop iteration:", i, sep = " "))

    # loop parameters
    loop_n <- if (length(n) > 1) n[i] else n

    # if i > 1 then exclude previous ids from previous lists
    if (i > 1) {
      # combine previous lists, de-dupe just in case
      exclusions <-
        list_output %>%
        do.call(rbind, .) %>%
        dplyr::select(dplyr::all_of(id)) %>%
        unlist(use.names = FALSE) %>%
        unique()

      # filter exclusions from data
      data <-
        data %>%
        dplyr::filter(!(!!sym(id) %in% exclusions))

      # verbosity
      if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "data deduped", sep = " "))
    }

    # error handling
    ## if loop_n > nrow(data) exit loop but continue onto next i
    if (loop_n > nrow(data)) {
      # verbosity
      if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "ERROR: loop_n > nrow(data)", sep = " "))
      # exit loop but continue to next iteration
      next
    }

    ## if nrow(data) < min_n exit loop but continue
    if (nrow(data) < min_n) {
      # verbosity
      if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "ERROR: nrow(data) < min_n", sep = " "))
      # exit loop but continue to next iteration
      next
    }

    # temporary step, stratified sample, list(i)
    step_loop_stratified_sample <- stratified_sampling(
      data = data,
      variables = dplyr::all_of(variables),
      n = loop_n,
      Wh = step_01$Wh,
      min_n = min_n
    )

    # verbosity
    if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "stratified sampling", sep = " "))

    step_loop_sampling_stats <- stratified_sampling_statistics(
      stratified_sample = step_loop_stratified_sample,
      variables = dplyr::all_of(variables)
    )

    # verbosity
    if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "sampling statistics", sep = " "))

    # out: insert loop into list outputs
    list_output[[i]] <- step_loop_stratified_sample
    list_statistics[[i]] <- step_loop_sampling_stats
    list_requested_n[[i]] <- attr(step_loop_stratified_sample, "n")

    # verbosity
    if (verbosity) message(paste("Step 03: loop iteration:", i, "...", "values inserted into lists", sep = " "))

    # update progress bar if...
    ## a) progressBar is not null AND
    ## b) progressBar = is a function
    ### all = Given a set of logical vectors, is at least one of the values true?
    all(!is.null(progressBar), is.function(progressBar)) %>%
      if (.) {
        progressBar(value = i)
      }

    # verbosity
    if (verbosity) message("Progress bar updated")
  }

  # verbosity
  if (verbosity) message("Step 03: Loop done")
  ###################################################
  # filter NULL list elements
  list_output <- Filter(f = function(x) !is.null(x), list_output)
  list_statistics <- Filter(f = function(x) !is.null(x), list_statistics)

  # verbosity
  if (verbosity) message("Step 04: Null list elements filtered out")
  ###################################################
  # summarize output: sample statistics: inner join
  samples_statistics <- Reduce(
    function(...) merge(..., by = {{ variables }}, all = FALSE),
    list_statistics
  )

  # verbosity
  if (verbosity) message("Step 05: Sample statistics combined")
  ###################################################
  ## rename merged variables
  names(samples_statistics)[-seq_along(variables)] <- paste(rep(c("nh", "Wh"), times = m), rep(names(list_output), each = 2), sep = "_")

  ## merge stats from parent file
  samples_statistics <- merge(
    # pop Wh
    step_01,
    # samples Wh
    samples_statistics,
    by = {{ variables }},
    all = FALSE
  ) %>%
    # round float/decimals 4 digits
    dplyr::mutate(dplyr::across(.cols = where(is.numeric), .fns = round, digits = 4)) %>%
    # arrange the columns so it's easier for users to read sampling stats table
    dplyr::select({{ variables }}, dplyr::contains("nh"), dplyr::contains("wh")) %>%
    # format n, nh, N, Nh
    dplyr::mutate(dplyr::across(.cols = dplyr::contains("nh", ignore.case = TRUE), .fns = as.integer)) %>%
    dplyr::mutate(dplyr::across(.cols = dplyr::contains("nh", ignore.case = TRUE), .fns = format, big.mark = ","))

  # verbosity
  if (verbosity) message("Step 06: Append pop statistics to sample stats")
  ###################################################
  # Create output: actual sample sizes

  ## calculate sample sizes per list
  actual_sample_sizes <-
    lapply(X = list_output, FUN = nrow) %>%
    as.data.frame()

  ## update names
  names(actual_sample_sizes) <- paste("n", names(actual_sample_sizes), sep = "_")

  ## sort columns
  actual_sample_sizes <-
    actual_sample_sizes %>%
    dplyr::mutate(dplyr::across(.cols = dplyr::everything(), .fns = format, big.mark = ","))

  # verbosity
  if (verbosity) message("Step 07: Actual sample sizes calculated")
  ###################################################
  # Create output: requested/modified sample sizes

  ## unlist
  requested_sample_sizes <-
    do.call(cbind, list_requested_n) %>%
    data.frame() %>%
    dplyr::mutate(dplyr::across(.cols = dplyr::everything(), .fns = format, big.mark = ","))

  ## update names: take from above
  names(requested_sample_sizes) <- names(actual_sample_sizes)

  # verbosity
  if (verbosity) message("Step 08: Requested sample sizes calculated")
  ###################################################
  # output
  list(
    sample_statistics = samples_statistics,
    output = list_output,
    actual_n = actual_sample_sizes,
    requested_n = requested_sample_sizes
  ) %>%
    return()
}
###################################################
###################################################
###################################################
###################################################
# Function definition: This is a function to create
# a simple random sample from a provided data set

# The output of this function is a list with x elements.
# Elements:
# - output
# - actual_n
# - requested_n

COLT_srs <- function(data,
                     m,
                     n,
                     id,
                     progressBar = NULL,
                     verbosity = TRUE,
                     ...) {
  ###################################################
  # verbosity
  if (verbosity) message("Initiate simple random sampling")
  ###################################################
  # create empty output objects
  list_output <- vector(mode = "list", length = m)
  names(list_output) <- paste("list", seq_len(m), sep = "_")
  ###################################################
  # verbosity
  if (verbosity) message("Step 01: start looping")

  ## loop through 1:m lists
  for (i in seq_len(m)) {
    # verbosity
    if (verbosity) message(paste("Step 01: loop iteration:", i, sep = " "))

    # loop parameters
    loop_n <- if (length(n) > 1) n[i] else n
    condition_1 <- loop_n > nrow(data)

    # if i > 1 then exclude previous ids from previous lists
    if (i > 1) {
      # combine previous lists, de-dupe just in case
      exclusions <-
        list_output %>%
        do.call(rbind, .) %>%
        dplyr::select(dplyr::all_of(id)) %>%
        unlist(use.names = FALSE) %>%
        unique()

      # filter exclusions from data
      data <-
        data %>%
        dplyr::filter(!(!!sym(id) %in% exclusions))

      # verbosity
      if (verbosity) message(paste("Step 01: loop iteration:", i, "...", "data deduped", sep = " "))
    }

    # error handling
    ## if loop_n > nrow(data) exit loop but continue onto next i
    if (condition_1) {
      # verbosity
      if (verbosity) message(paste("Step 01: loop iteration:", i, "...", "ERROR: loop_n > nrow(data)", sep = " "))
      # exit loop but continue to next iteration
      next
    }

    # simple random sample (srs) &
    # insert loop into list outputs
    set.seed(123)
    list_output[[i]] <-
      data %>%
      dplyr::slice_sample(n = loop_n) %>%
      dplyr::as_tibble()

    # verbosity
    if (verbosity) message(paste("Step 01: loop iteration:", i, "...", "SRS and inserted into list", sep = " "))

    # update progress bar if...
    ## a) progressBar is not null AND
    ## b) progressBar = is a function
    ### all = Given a set of logical vectors, is at least one of the values true?
    all(!is.null(progressBar), is.function(progressBar)) %>%
      if (.) {
        progressBar(value = i)
      }

    # verbosity
    if (verbosity) message("Progress bar updated")
  }

  # verbosity
  if (verbosity) message("Step 01: end looping")
  ###################################################
  # Create output: actual sample sizes

  ## calculate sample sizes per list
  actual_sample_sizes <-
    lapply(X = list_output, FUN = nrow) %>%
    as.data.frame()

  ## update names
  names(actual_sample_sizes) <- paste("n", names(actual_sample_sizes), sep = "_")

  ## sort columns
  actual_sample_sizes <-
    actual_sample_sizes %>%
    dplyr::mutate(dplyr::across(.cols = dplyr::everything(), .fns = format, big.mark = ","))

  # verbosity
  if (verbosity) message("Step 02: Actual sample sizes calculated")
  ###################################################
  # Create output for requested n
  ## conditional: if n = 1 but m > 1 repeat n m times else n
  requested_n <- if (length(n) == 1) rep(n, times = m) else n
  ## put into data frame
  requested_n <-
    matrix(requested_n, nrow = 1, ncol = m) %>%
    as.data.frame() %>%
    ## format
    dplyr::mutate(dplyr::across(.cols = dplyr::everything(), .fns = format, big.mark = ","))
  ## give data frame names, taken from above
  names(requested_n) <- names(actual_sample_sizes)
  # verbosity
  if (verbosity) message("Step 03: Requested sample sizes calculated")
  ###################################################
  # output
  list(
    output = list_output,
    actual_n = actual_sample_sizes,
    requested_n = requested_n
  ) %>%
    return()
}
###################################################
###################################################
###################################################
###################################################

# EXAMPLE: not run
# example =
#   COLT(
#     data = data,
#     variables = c("service_type", "gender", "marital_status"),
#     n = c(5000, 15000),
#     m = 2,
#     id = "contract_account"
#   )
###################################################
###################################################
###################################################
###################################################
# TESTING/ DEBUGGING

# import data
# foo = arrow::read_feather(file = file.path(usethis::proj_get(), "data", "sample.feather"))

# stratified sampling
# strata = c("county_service")#, "gender","income_range", "age_range")
# bar = stratified_proportional_allocation(data = foo, variables= tidyselect::all_of(strata))
# spam = stratified_sampling(data = foo, variables = tidyselect::all_of(strata), n = 2000000, Wh = bar$Wh, min_n = attr(bar, "min_n"))
# xxx = stratified_sampling_statistics(stratified_sample = spam, variables= tidyselect::all_of(strata))
# test = COLT_stratified_prop_alloc(
#   data = foo,
#   variables = tidyselect::all_of(strata),
#   n = 3000000,
#   m = 2,
#   id = "contract_account"
# )

# SRS
# COLT_srs(
#   data = foo,
#   m = 4,
#   n = 250000,
#   id = "contract_account",
#   verbosity = TRUE
# )
